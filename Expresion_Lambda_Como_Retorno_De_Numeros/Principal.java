public class Principal {
    public static void main(String[] args){
		
		System.out.println("==> CalculadoraInt --> resultado = " + Principal.engine_1().calculate(5,5));
		System.out.println("==> CalculadoraLong --> resultado = " + Principal.engine_2().calculate(6,2));
		System.out.println("==> CalculadoraLong --> resultado = " + Principal.engine_3().calculate(5,3));
		System.out.println("==> CalculadoraInt --> resultado = " + Principal.engine_4().calculate(7,0));
	}

	// Retorna un objeto de tipo "CalculadoraInt"
	private static CalculadoraInt engine_1(){
		return (x, y) -> x * y;
	}

	// Retorna un objeto de tipo "CalculadoraLong"
	private static CalculadoraLong engine_2(){
		return (x, y) -> x - y;
	}
	// Retorna un objeto de tipo "CalculadoraLong"
	private static CalculadoraLong engine_3(){
		return (x, y) -> x + y;
	}
	// Retorna un objeto de tipo "CalculadoraInt"
	private static CalculadoraInt engine_4(){
			return (x, y) -> x / y;	
			//Posible Solución a la Division
			/*try {
				// Division Por cero
				if (y!==0)
				  throw new ArithmeticException("No se Puede Dividir Entre Cero");
				return x/y; // if b! = 0, Entonces Devolvera El Resultado De La Division
			  }
			  catch (ArithmeticException e) {
				System.out.println(e.getMessage()); // Imprime La Excepción
				return (x/y);
			  }*/
	}
}
